-- ****************************** --
-- User: MechWipf
-- Date: 21.07.2015
-- Time: 17:27
-- ****************************** --


-- Simple per request cache, speeds up some things, and requirered files shouldn't change while requesting
do
    local cache = {}
    local _require = getfenv(0).require
    getfenv(0).require = function ( s, noCache )
        if not cache[s] and not noCache then
            cache[s] = _require( s )
        end

        return cache[s]
    end
end

do
    local _G = getfenv(0)

    require "libs.linq"

    _G.from = from
    _G.ngx = require "ngx"
    _G.auth = require "app.user.auth"
    _G.lazy = require "libs.utils".lazyLoad
    _G.utils = require "libs.utils"
    _G.colors = require "ansicolors"
    _G.db = require "lapis.db"
end

local Router = Router
local string = string
local require = require
local table = table
local math = math
local pcall = pcall
local os = os
local auth = auth

--local ngx = require "ngx"
local lapis = require "lapis"
--local sha1 = require "sha1"
local app = lapis.Application()
local respond_to = lapis.application.respond_to
--local plugins = {}

math.randomseed( os.time() )
for _ = 1, 10 do math.random() end

-- Routes --
local Router = require "app.shared.router" ( app )
local baseUrl = "data"

Router:route( baseUrl, "devices", require "app.device.controller" )
Router:route( baseUrl, "users", require "app.user.controller" )
Router:route( baseUrl, "serials", require "app.serial.controller" )
-- ------ --

function string:split(sep)
    local sep, fields = sep or ":", {}
    local pattern = string.format("([^%s]+)", sep)
    self:gsub(pattern, function(c) fields[#fields+1] = c end)
    return fields
end

function table:getValue ( str )
    local t = self

    for _, v in pairs( str:split( '.' ) ) do
        t = t[v]
    end

    return t
end

app:enable( "etlua" )

app:before_filter( function( self )
        local security = {
        "default-src: self",
        "script-src: self",
        "sass-src: self",
        "img-src: self"
    }
    self.res.headers["Content-Security-Policy"] = table.concat( security, "; " )

    if auth.check() then
        app.user = auth.current_user
    end
end )

app:post( "auth", "/auth", function ( self )
    local jwt = auth.authenticate( self.params )

    if not jwt then
        return { json = { message = "Credentials are not accepted." }, status = 400 }
    else
        return { json = { jwt = jwt } }
    end
end )

app:post( "authRequired", "/auth_required", function ( _ )
    return { json = { auth_required = auth.check() == false } }
end )

app:post( "registration", "/register", function ( self )
    return require "app.user.controller":registration( self.params )
end )

app:match( "login", "/", respond_to(
    {
        POST = function ( self )
            if auth.check( self.params.jwt ) then
                return { render = "components.app.mainLayout", layout = false }
            else
                return { render = "components.login.loginLayout", layout = false }
            end
        end,

        GET = function ( _ )
            if auth.check() then
                return { render = "components.app.mainLayout", layout = false }
            else
                return { render = "components.login.loginLayout", layout = false }
            end
        end
    }
))

app:get( "views", "/view/:view_name", function ( self )

    local o = {
        pcall( function ()
            return require( 'views.' .. self.params.view_name )
        end )
    }

    if not o[1] then
        return { render = "notfound", layout = false }
    else
        return { render = o[2], layout = false }
    end
end )


do
    local console = require "lapis.console"

    app:match("/console", console.make())
end

function app.userIsAdmin ()
    local Permission = require "app.user.permission"
    local isAdmin
    local perm

    perm = Permission.get( app.user, "users", "read", "*" )
    if perm and #perm > 0 then
        isAdmin = true
    end

    perm = Permission.get( app.user, "devices", "read", "*" )
    if perm and #perm > 0 then
        isAdmin = true
    end

    return isAdmin
end

return app
