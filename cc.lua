#!/usr/bin/env lua
--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 16.02.2016
-- Time: 15:12
-- To change this template use File | Settings | File Templates.
--
local util = require "lapis.cmd.util"
local nginx = require "lapis.cmd.nginx"
local colors = require "ansicolors"

local print = print
local pcall = pcall
local arg = arg
local server

if not nginx.get_pid() then
    print( colors "%{green}Using temporary server..." )
else
    print( colors( "%{green}Attaching to server: %{yellow}" .. nginx.get_pid() ) )
end


local str = ("./ccommands/%s.lua"):format(arg[1])

print( str )

local f = io.open( str , "r" )
local code = f:read"*a" f:close()

local function main ()
    table.remove( arg, 0 )
    table.remove( arg, 1 )
    print( server:exec( code:format( unpack( arg ) ) ) )
end



server = nginx.attach_server( util.default_environment() )

local ok, err = pcall(main)

if not ok then
    print( err )
end

server:detach()