/* Created by MechWipf on 25.07.2015. */

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    changed = require('gulp-changed'),
    filelog = require('gulp-filelog'),
    clean = require('gulp-rimraf'),
    addsrc = require('gulp-add-src');


var files = {
    js: {
        vendor: [
            'bower_components/jquery/dist/jquery.js',
            'bower_components/underscore/underscore.js',
            'bower_components/jquery-mousewheel/jquery.mousewheel.js',
            'bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js',
            'bower_components/angular/angular.js',
            'bower_components/angular-ui-router/release/angular-ui-router.js',
            'bower_components/angular-materialize/src/angular-materialize.js',
            'bower_components/restangular/dist/restangular.js',
            'bower_components/angular-material-icons/angular-material-icons.js',
            'bower_components/ng-scrollbars/src/scrollbars.js',
            'bower_components/materialize/js/**/*.js',
            'bower_components/angular-jwt/dist/angular-jwt.js',
            '!**/date_picker/*.js'
        ],
        app: [
            'www/js/app/app.js',
            'www/js/app/**/*.js',
            'www/js/shared/**/*.js'
        ],
        login: [
            'www/js/login/login.js',
            'www/js/login/**/*.js',
            'www/js/shared/**/*.js'
        ],
        dest: 'static/js/'
    },
    sass: {
        vendor: [
            'bower_components/materialize/sass/materialize.scss'
        ],
        app: [
            'www/sass/app.sass'
        ],
        internetExplorer: [
            'www/sass/ie11.sass'
        ],
        dest: 'static/css/'
    },
    css: {
        vendor: [
            'bower_components/angular-material-icons/angular-material-icons.css',
            'bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css'
        ]
    },
    font: {
        vendor: [
            'www/font/**'
        ],
        dest: 'static/font/'
    },
    images: {
        app: [
            'www/images/**'
        ],
        dest: 'static/images/'
    }
};

gulp.task('sass:clean', function () {
   return gulp.src(files.sass.dest)
       .pipe(clean());
});

gulp.task('js:clean', function () {
    return gulp.src(files.js.dest)
        .pipe(clean());
});

gulp.task('font:clean', function () {
    return gulp.src(files.font.dest)
        .pipe(clean());
});

gulp.task('images:clean', function () {
   return gulp.src(files.images.dest)
       .pipe(clean());
});

gulp.task('sass:app', ['sass:clean'], function() {
    return gulp.src(files.sass.app)
        .pipe(sass())
        .pipe(concat('app.css'))
        .pipe(changed(files.sass.dest))
        .pipe(filelog('sass:app'))
        .pipe(gulp.dest(files.sass.dest));
});

gulp.task('sass:vendor', ['sass:clean'], function () {
    return gulp.src(files.sass.vendor)
        .pipe(sass())
        .pipe(addsrc.append(files.css.vendor))
        .pipe(concat('vendor.css'))
        .pipe(changed(files.sass.dest))
        .pipe(filelog('sass:vendor'))
        .pipe(gulp.dest(files.sass.dest));
});

gulp.task('sass:watch', ['sass:app', 'sass:vendor'], function() {
    return gulp.watch('www/sass/**', ['sass:app', 'sass:vendor']);
});

gulp.task('sass', ['sass:app', 'sass:vendor']);


gulp.task('js:app', ['js:clean'], function () {
    return gulp.src(files.js.app)
        .pipe(concat('app.js'))
        .pipe(changed(files.js.dest))
        .pipe(filelog('js:app'))
        .pipe(gulp.dest(files.js.dest));
});

gulp.task('js:login', ['js:clean'], function () {
    return gulp.src(files.js.login)
        .pipe(concat('login.js'))
        .pipe(changed(files.js.dest))
        .pipe(filelog('js:login'))
        .pipe(gulp.dest(files.js.dest));
});

gulp.task('js:vendor', ['js:clean'], function() {
    return gulp.src(files.js.vendor)
        .pipe(concat('vendor.js'))
        .pipe(changed(files.js.dest))
        .pipe(filelog('js:vendor'))
        .pipe(gulp.dest(files.js.dest));
});

gulp.task('js:watch', ['js:app', 'js:vendor'], function () {
    return gulp.watch('www/js/**', ['js:app', 'js:login', 'js:vendor']);
});

gulp.task('js', ['js:app', 'js:login', 'js:vendor']);


gulp.task('font', ['font:clean'], function() {
   gulp.src(files.font.vendor)
       .pipe(changed(files.font.dest))
       .pipe(filelog('font:vendor'))
       .pipe(gulp.dest(files.font.dest));
});

gulp.task('images', ['images:clean'], function() {
   gulp.src(files.images.app)
       .pipe(changed(files.images.dest))
       .pipe(filelog('images:app'))
       .pipe(gulp.dest(files.images.dest));
});

gulp.task('watch', ['sass:watch', 'js:watch']);


gulp.task('default', [
    'sass:clean',
    'js:clean',
    'font:clean',
    'images:clean',
    'sass',
    'js',
    'font',
    'images',
    'js:watch',
    'sass:watch'
]);