/**
 * Created by MechWipf on 12.08.2015.
 */

angular.module('app')
    .directive('ccDevice', function () {
        return {
            restrict: 'E',
            scope: {
                item: '='
            },
            templateUrl: 'view/components.device.deviceDirective'
        }
    });