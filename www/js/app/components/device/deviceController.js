/**
 * Created by MechWipf on 30.08.2015.
 */
'use strict';

angular.module('app')
    .controller('DeviceController', ['$scope', '$stateParams', '$state', 'Devices', 'Restangular',
        function ($scope, $stateParams, $state, Devices, Restangular) {

            $scope.stateParams = $stateParams;

            Devices.one($stateParams.id).get({'with': '["guid"]'})
                .then(function (device) {
                    $scope.device = device;
                });

            $scope.showDrop = function (device) {
                $scope.refObject = device;
                $('#removeModal').openModal();
            };

            $scope.drop = function () {
                $scope.refObject.remove().then(
                    function (ref) {
                        $state.go('devices')
                    }
                );
            };

            $scope.cancle = function () {
                $scope.refObject = null;
            };

            $scope.showEdit = function () {
                $scope.editObject = Restangular.copy($scope.device);
                $('#editModal').openModal();
            };

            $scope.apply = function (obj) {
                obj.save({'with': '["guid"]'}).then(
                    function (res) {
                        $scope.device = res;
                    }
                );
            }


        }
    ]);