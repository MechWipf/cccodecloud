/**
 * Created by MechWipf on 13.09.2015.
 */
'use strict';

angular.module('app')
    .factory('Devices', ['Restangular',
        function (Restangular) {
            return Restangular.service('devices');
        }
    ]);