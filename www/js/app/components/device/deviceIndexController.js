'use strict';

angular.module('app')
    .controller('DeviceIndexController', ['$scope', 'Devices',
        function ($scope, Devices) {
            $scope.newObject = {};


            Devices.getList({'with': '["guid"]'})
                .then(function (devices) {
                    $scope.devices = devices;
                });

            $scope.showDrop = function (device) {
                $scope.refObject = device;
                $('#removeModal').openModal();
            };

            $scope.drop = function () {
                $scope.refObject.remove().then(
                    function (ref) {
                        $scope.devices = _.without($scope.devices, $scope.refObject);
                    },
                    function (res) {
                        alert('Error')
                    }
                );
            };

            $scope.cancle = function () {
                $scope.refObject = null;
            };

            $scope.showNew = function () {
                $scope.newObject = {};

                $('#newModal').openModal();
            };

            $scope.new = function (object) {
                Devices.post(object, {'with': '["guid"]'}).then(
                    function (res) {
                        $scope.devices.push(res)
                    },
                    function (res) {
                        alert('Error')
                    }
                )
            }
        }
    ]);