/**
 * Created by MechWipf on 26.11.2015.
 */
'use strict';

angular.module('app')
    .factory('UserJW', ['jwtHelper',
        function (jwtHelper) {
            return {
                get: function () {
                    return jwtHelper.decodeToken(localStorage.getItem('id_token'));
                }
            }
        }
    ])
    .factory('Users', ['Restangular',
        function (Restangular) {
            return Restangular.service('users');
        }]);