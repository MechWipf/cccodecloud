/**
 * Created by MechWipf on 07.03.2016.
 */
'use strict';

angular.module('app')
    .controller('UserIndexController', ['$scope', 'Users',
        function ($scope, Users) {
            $scope.newObject = {};

            Users.getList()
                .then(function (users) {
                    $scope.users = users;
                });

            $scope.showDrop = function (device) {
                $scope.refObject = device;
                $('#removeModal').openModal();
            };

            $scope.drop = function () {
                $scope.refObject.remove().then(
                    function (ref) {
                        $scope.devices = _.without($scope.devices, $scope.refObject);
                    },
                    function (res) {
                        alert('Error')
                    }
                );
            };

            $scope.cancle = function () {
                $scope.refObject = null;
            };

            $scope.showNew = function () {
                $scope.newObject = {};

                $('#newModal').openModal();
            };
        }
    ]);