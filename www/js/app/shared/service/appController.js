/**
 * Created by MechWipf on 16.08.2015.
 */

'use strict';

angular.module('app')
    .controller('AppController', ['$scope', 'UserJW',
        function ($scope, UserJW) {
            $scope.user = UserJW.get();

            $scope.logout = function () {
                localStorage.removeItem('id_token');
                window.location = '/';
            };
        }
    ]);