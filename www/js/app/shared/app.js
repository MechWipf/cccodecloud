'use strict';

angular.module('app', ['ui.materialize', 'ui.router', 'ngMdIcons', 'ngScrollbars', 'restangular', 'angular-jwt'])
    .run(['Restangular',
        function (Restangular) {
            Restangular
                .setBaseUrl('/data');

            var clean_uri = location.protocol + "//" + location.host + location.pathname;

            var hash_pos = location.href.indexOf("#");
            if (hash_pos > 0) {
                var hash = location.href.substring(hash_pos, location.href.length);
                clean_uri += hash;
            }

            window.history.replaceState({}, document.title, clean_uri);
        }
    ])
    .config(function ($httpProvider, jwtInterceptorProvider) {

        jwtInterceptorProvider.tokenGetter = function () {
            return localStorage.getItem('id_token');
        };

        $httpProvider.interceptors.push('jwtInterceptor');
    });
