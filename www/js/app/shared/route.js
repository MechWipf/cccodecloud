'use strict';

angular.module('app')
    .config(['$stateProvider',
        function ($stateProvider) {
            $stateProvider
                .state('cc', {
                    url: '',
                    views: {
                        "header": {templateUrl: 'view/components.app.mainHeaderView'},
                        "main": {template: '<ui-view></ui-view>'}
                    }
                })
                .state('cc.files', {
                    url: '/files',
                    templateUrl: 'view/components.file.fileIndex'
                })
                .state('cc.devices', {
                    url: '/devices',
                    templateUrl: 'view/components.device.deviceIndex',
                    controller: 'DeviceIndexController'
                })
                .state('cc.device', {
                    url: '/device/:id',
                    templateUrl: 'view/components.device.deviceMainView',
                    controller: 'DeviceController'
                })

                .state('admin', {
                    url: '/admin',
                    views: {
                        "header": {templateUrl: 'view/components.admin.adminHeaderView'},
                        "main": {template: '<ui-view></ui-view>'}
                    }
                })
                .state('admin.users', {
                    url: '/users',
                    templateUrl: 'view/components.user.userIndex',
                    controller: 'UserIndexController'
                })
                .state('admin.user', {
                    url: 'user/:id',
                    templateUrl: 'view/components.user.userMainView',
                    controller: 'UserController'
                })

                .state('impressum', {
                    url: '/impressum',
                    views: {
                        "header": {templateUrl: 'view/components.impressum.impressumHeaderView'},
                        "main": {templateUrl: 'view/components.impressum.impressumMainView'}
                    }
                })
        }]);