/**
 * Created by MechWipf on 12.11.2015.
 */

'use strict';

angular.module('login')
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('login', {
                url: '/',
                templateUrl: 'view/components.login.loginMainView'
            })
            .state('register', {
                url: '/register',
                templateUrl: 'view/components.login.register',
                controller: 'RegisterController'
            })
            .state('impressum', {
                url: '/impressum',
                templateUrl: 'view/components.impressum.impressumMainView'
            })
    }]);