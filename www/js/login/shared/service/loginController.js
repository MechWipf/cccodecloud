/**
 * Created by MechWipf on 12.11.2015.
 */

'use strict';

angular.module('login')
    .controller('LoginController', ['$rootScope', '$scope', '$http', '$timeout',
        function ($rootScope, $scope, $http, $timeout) {
            $scope.jwt = '';

            $rootScope.login = function (jwt) {
                if (jwt != null) {
                    localStorage.setItem('id_token', jwt);
                    $scope.jwt = jwt;
                } else {
                    $scope.jwt = localStorage.getItem('id_token');
                }

                $timeout(function () {
                    $('#hidden_form').submit();
                }, 1000);
            };

            $scope.doLogin = function (values) {
                if (values == null || (values.password == "" && values.username == "")) {
                    Materialize.toast('Enter Username and Password!', 2000);
                    return false;
                } else if (values.username == null || values.username == "") {
                    Materialize.toast('Enter Username!', 2000);
                    return false;
                } else if (values.password == null || values.password == "") {
                    Materialize.toast('Enter Password!', 2000);
                    return false;
                }

                $http({
                    method: 'POST',
                    url: '/auth',
                    data: {
                        name: values.username,
                        password: values.password
                    }
                }).then(function ( response ) {
                    $rootScope.login( response.data.jwt );
                }, function () {
                    Materialize.toast('Username or Password wrong!', 2000);
                    values.password = '';
                })
            };

            $http({
                method: 'POST',
                url: '/auth_required'
            }).then(
                function ( req ) {
                    if (req.data.auth_required === false) {
                        $rootScope.login();
                    }
                }
            );
        }
    ]);