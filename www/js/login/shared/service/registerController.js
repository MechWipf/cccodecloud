/**
 * Created by MechWipf on 05.12.2015.
 */

'use strict';

angular.module('login')
    .controller('RegisterController', ['$scope', '$state', '$http', '$timeout',
        function ($scope, $state, $http, $timeout) {

            $scope.doRegister = function (values) {
                $http({
                    method: 'POST',
                    url: '/register',
                    data: values
                }).then(function (response) {
                    if (response.data.success === true) {
                        Materialize.toast("Registration successful!", 2000);

                        $timeout(function () {
                            $state.go('login');
                        }, 2000);
                    } else {
                        var message = "Registration failed!";

                        if (response.data.message) {
                            message = response.data.message;
                        }

                        Materialize.toast(message, 2000);
                    }
                })
            };

        }
    ]);