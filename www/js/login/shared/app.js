/**
 * Created by MechWipf on 12.11.2015.
 */

'use strict';

angular.module('login', ['ui.materialize', 'ui.router', 'restangular', 'angular-jwt'])
    .run(['$state',
        function ($state) {
            Vel = $.Velocity;

            $state.go('login');
        }
    ])
    .config(function ($httpProvider, jwtInterceptorProvider) {

        jwtInterceptorProvider.tokenGetter = function () {
            return localStorage.getItem('id_token');
        };

        $httpProvider.interceptors.push('jwtInterceptor');
    });
