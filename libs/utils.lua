local m = {}

function m.printTable(t, tabs)
    if not t then return end
    if not tabs then tabs = 0 end

    for k,v in pairs(t) do
        local data = string.rep('\t', tabs)..tostring(k)..' = '

        if type(v) == 'table' and tabs < 10 then
            print(data..'{')
            m.printTable(v, tabs+1)
            print(string.rep('\t', tabs)..'}')
        else
            print(data..tostring(v))
        end
    end
end

function m.serialize( t )
    local sb = {}

    local old_print = print
    print = function (...)
        sb[#sb+1] = table.concat({...}, "  ")
    end

    m.printTable( t )

    print = old_print

    return table.concat( sb, "\n" )
end

function m.lazyLoad ( s )
    return function() return require( s ) end
end

return m