--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 13.12.2015
-- Time: 15:59
-- To change this template use File | Settings | File Templates.
--

local Serial = require "app.serial.model"
local Guid = require "app.guid.model"

local serial = Serial:create{ type = 1, deleted_at = nil, data = [[{"role":%q}]] }

print( Guid:find{ x_id = serial.id, x_type = "serials" }.guid )