
### Kiwigaming presents the
# ComputerCraft Code Cloud

For all of you who likes to save your projects online for instant access by every computer in minecraft.


    OPENRESTY_VERSION=1.9.3.1
    OPENRESTY_PREFIX=/opt/openresty
    NGINX_PREFIX=/opt/openresty/nginx
    VAR_PREFIX=/var/nginx
    LUAROCKS_VERSION=2.2.2
    LUA_JIT_VERSION=2.0.4
    PATH=$OPENRESTY_PREFIX/luajit/bin:$PATH
    
    # NginX prefix is automatically set by OpenResty to $OPENRESTY_PREFIX/nginx
    # look for $ngx_prefix in https://github.com/openresty/ngx_openresty/blob/master/util/configure
    
    echo "==> Installing dependencies..." \
    && apk update \
    && apk add make gcc musl-dev \
      pcre-dev openssl-dev zlib-dev \
      ncurses-dev readline-dev \
      curl perl unzip
    
    mkdir -p /app
    
    mkdir -p /root/ngx_openresty \
    && cd /root/ngx_openresty \
    && echo "==> Downloading OpenResty..." \
    && curl -sSL http://openresty.org/download/ngx_openresty-${OPENRESTY_VERSION}.tar.gz | tar -xvz \
    && cd ngx_openresty-* \
    && echo "==> Configuring OpenResty..." \
    && readonly NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
    && echo "using upto $NPROC threads" \
    && ./configure \
      --prefix=$OPENRESTY_PREFIX \
      --http-client-body-temp-path=$VAR_PREFIX/client_body_temp \
      --http-proxy-temp-path=$VAR_PREFIX/proxy_temp \
      --http-log-path=$VAR_PREFIX/access.log \
      --error-log-path=$VAR_PREFIX/error.log \
      --pid-path=$VAR_PREFIX/nginx.pid \
      --lock-path=$VAR_PREFIX/nginx.lock \
      --with-luajit \
      --with-pcre-jit \
      --with-ipv6 \
      --with-http_ssl_module \
      --without-http_ssi_module \
      --without-http_userid_module \
      --without-http_uwsgi_module \
      --without-http_scgi_module \
      -j${NPROC} \
    && echo "==> Building OpenResty..." \
    && make -j${NPROC} \
    && echo "==> Installing OpenResty..." \
    && make install \
    && echo "==> Finishing..." \
    && ln -sf $NGINX_PREFIX/sbin/nginx /usr/local/bin/nginx \
    && ln -sf $NGINX_PREFIX/sbin/nginx /usr/local/bin/openresty \
    && ln -sf $OPENRESTY_PREFIX/bin/resty /usr/local/bin/resty \
    && ln -sf $OPENRESTY_PREFIX/luajit/bin/luajit-* $OPENRESTY_PREFIX/luajit/bin/lua \
    && ln -sf $OPENRESTY_PREFIX/luajit/bin/luajit-* /usr/local/bin/lua \
    && cd ~ \
    && rm -rf /var/cache/apk/* \
    && rm -rf /root/ngx_openresty
    
    mkdir -p /root/luarocks \
    && cd /root/luarocks \
    && echo "==> Downloading Luarocks" \
    && curl -sSL http://keplerproject.github.io/luarocks/releases/luarocks-$LUAROCKS_VERSION.tar.gz | tar -xvz \
    && cd luarocks-* \
    && ./configure \
      --lua-version=5.1 \
      --with-lua=$OPENRESTY_PREFIX/luajit \
      --with-lua-include=$OPENRESTY_PREFIX/luajit/include/luajit-2.1 \
      --prefix=$OPENRESTY_PREFIX/luajit \
    && make build \
    && make install \
    && cd ~ \
    && rm -rf /root/luarocks
    
    luarocks install luasocket \
    && luarocks install luasec \
    && luarocks install uuid \
    && luarocks install sha1 \
    && luarocks install lapis \
    && luarocks install moonscript \
    && luarocks install lapis-console
    
    echo "==> Cleanup" \
    && apk del \
      make gcc musl-dev pcre-dev openssl-dev zlib-dev ncurses-dev readline-dev curl perl \
    && apk update \
    && apk add \
      libpcrecpp libpcre16 libpcre32 openssl libssl1.0 pcre libgcc libstdc++ \
    && apk cache clean -f
    
    cd $NGINX_PREFIX/