--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 13.02.2016
-- Time: 16:01
-- To change this template use File | Settings | File Templates.
--
local auth = auth
local os = os

local json = require "cjson"
local BaseController = require "app.shared.base_controller"
local UserController = setmetatable( {}, { __index = BaseController } )

UserController.base = BaseController
UserController.model = lazy "app.user.model"

function UserController:registration( params )
    local Serial = require "app.serial.model"
    local Guid = require "app.guid.model"
    local User = self.model()

    if not params.username or #params.username < 2 then return { json = { message = "Username must be at least 3 chars long." }, status = 400 } end
    if User:find{ name = params.username } then return { json = { message = "Username already in use." }, status = 409 } end
    if params.password ~= params.password_confirm then return { json = { message = "Password and confirmation have to match." }, status = 400 } end

    local guid = Guid:find{ guid = params.pass }
    if not guid then return { json = { message = "Unknown Registration-Pass used." }, status = 400 } end

    local serial = Serial:find{ id = guid.x_id, deleted_at = nil }
    if not serial then return { json = { message = "Unknown Registration-Pass used." }, status = 400 } end

    local options = json.decode( serial.data )

    User:create{ name = params.username, password = auth.makeHash( params.password ), role = options.role }
    serial:update{ deleted_at = os.date "%x" }

    return { json = { success = true } }
end

return UserController