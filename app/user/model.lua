--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 03.08.2015
-- Time: 21:42
-- To change this template use File | Settings | File Templates.
--

local unpack = unpack

local Permission = require "app.user.permission"
local lazy = require "libs.utils".lazyLoad
local Model = require "app.shared.model_extended".ModelEX

local roles

local User = Model:extend("users", {
    primary_key = "id",

    devices = { model = lazy "app.device.model", relation = { "hasMany", "id", "user_id" } },
    guid = { model = lazy "app.guid.model", relation = { "morphOne", "x_id", "id" } },

    validate = {
        { "name", exists = true, min_length = 3, max_length = 250 },
        { "password", exists = true, min_length = 8 }
    },

    updateFilter = { name = true, password = true },

    exportFilter = { id = true, name = true },

    onCreate = function( values )
        local role = values.role
        values.role = nil

        if not role then
            ngx.log( ngx.WARN, colors "%{red}Error: No role selected" )
            return false
        end

        local ok, err = pcall( function ()
            local file = "app/user/roles/" .. role .. ".role.lua"
            roles = dofile( file )

            ngx.log( ngx.WARN, "Meep: " .. file )

            if not roles or not (#roles > 0) then
                error( "Can't read file:" .. file )
            end
        end )

        if not ok then
            ngx.log( ngx.WARN, colors( "%{red}Role not found:" .. tostring( err ) ) )
            return false
        end

        return true
    end,

    onCreated = function( self )
        self.guid.model():create { x_id = self.id, x_type = "users" }

        for _,v in pairs( roles ) do
            Permission.setPermission( self, unpack(v) )
        end

        roles = nil
    end
} )

return User