--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 18.01.2016
-- Time: 20:10
-- To change this template use File | Settings | File Templates.
--

local concat = table.concat
local pairs = pairs
local db = db

local permission = {}


function permission.get ( obj, perm, flags, filter )
    local sql = [[
  p.name    AS permission_name,
  px.filter AS filter,
  pt.name   AS flag
FROM permissions_x AS px
  INNER JOIN permissions AS p ON px.permission_id = p.id
                                 AND p.name = ?
  INNER JOIN permission_types AS pt ON p.permission_type_id = pt.id
WHERE pt.name IN (%s)
      AND px.x_id = ?
      AND px.x_type LIKE ?
      %s
]]

    flags = type(flags) == "table" and flags or { flags }
    for k, v in pairs( flags ) do
        flags[k] = ("'%s'"):format( v )
    end

    if filter then
        filter = type(filter) == "table" and filter or { filter }

        for k, v in pairs( filter ) do
            filter[k] = ("'%s'"):format( v )
        end

        sql = sql:format( concat( flags, ',' ), ("AND px.filter in (%s)"):format( concat( filter, "," ) ) )
    else
        sql = sql:format( concat( flags, ',' ), "" )
    end

    return db.select( sql, perm, obj.id, obj.__class.__name )
end

function permission.setPermission ( obj, perm, flag, filter )
    local permissions = permission.get( obj, perm, { flag }, { filter } )
    if permissions and (#permissions > 0) then return false end

    local sql = [[
  permissions.id as id
from permissions
  inner join permission_types on permission_types.id = permissions.permission_type_id
                                 and permission_types.name = ?
where permissions.name = ?;
]]


    local permission_id = db.select( sql, flag, perm )[1].id

    -- x_id, permission_id, x_type, value
    db.insert( "permissions_x", { x_id = obj.id, permission_id = permission_id, x_type = obj.__class.__name, filter = filter, value = true } )

    return true
end

function permission.delPermission ( obj, perm, flags, filter )

end

return permission