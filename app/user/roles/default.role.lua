--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 16.02.2016
-- Time: 18:54
-- To change this template use File | Settings | File Templates.
--

return {
    { "devices", "create", "own" },
    { "devices", "read"  , "own" },
    { "devices", "update", "own" },
    { "devices", "delete", "own" },

    { "files", "create", "own" },
    { "files", "read"  , "own" },
    { "files", "update", "own" },
    { "files", "delete", "own" },

}