local string = string
local ngx = ngx
local db = db or require "lapis.db"

local jwt = require "resty.jwt"
local config = require "lapis.config".get()
local sha1 = require "sha1"

local auth = {}

function auth.check ( token )

    if token == nil then
        local auth_header = ngx.var.http_Authorization
        local _

        if auth_header == nil then
            ngx.log(ngx.WARN, "No Authorization header")
            return false
        end

        ngx.log(ngx.INFO, "Authorization: " .. auth_header)

        _, _, token = string.find(auth_header, "Bearer%s+(.+)")
    end

    if token == nil then
        ngx.log(ngx.WARN, "Missing token")
        return false
    end

    local jwt_obj = jwt:verify(config.secret, token, 0)
    if jwt_obj.verified == false then
        ngx.log(ngx.WARN, "Invalid token: " .. jwt_obj.reason)
        return false
    end

    auth.current_user = require "app.user.model":find{ id = jwt_obj.payload.id, deleted_at = db.NULL }

    if not auth.current_user then
        return false
    end

    return true
end

function auth.authenticate ( values )
    local user = require "app.user.model":find{ name = values.name, deleted_at = db.NULL }

    if not user then
        return false
    end

    if not auth.checkHash( values.password, user.password or "" ) then
        return false
    end

    auth.current_user = user
    return auth.sign( { name = user.name, id = user.id } )
end

function auth.sign( tbl )
    local jwt_obj = {
        header = { alg = "HS256", typ = "JWT" },
        payload = tbl
    }

    return jwt:sign( config.secret, jwt_obj )
end

function auth.makeHash( passwd, salt )
    local salt = salt or sha1( 'i_am_salt' .. ngx.now() )
    return salt .. "$" .. sha1.hmac( config.secret .. salt, passwd )
end

function auth.checkHash( passwd, hash )
    local salt = hash:sub( 1, 40 )
    return auth.makeHash( passwd, salt ) == hash
end

return auth