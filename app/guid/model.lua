--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 04.10.2015
-- Time: 16:32
-- To change this template use File | Settings | File Templates.
--

local lazy = require "libs.utils".lazyLoad
local uuid = require "uuid"
local db = require "lapis.db"
local Model = require "app.shared.model_extended".ModelEX

uuid.randomseed( ngx.now() )

local GUID = Model:extend( "guids", {
    primary_key = { "id" },

    exportFilter = { guid = true },

    onCreate = function ( params )
        local guid

        repeat
            guid = uuid.new()
            local res = db.select( "id from guids where guid = ?", guid )
        until not (#res > 0)

        params.guid = guid

        return true
    end
} )

return GUID