--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 08.09.2015
-- Time: 15:42
-- To change this template use File | Settings | File Templates.
--

local ngx = ngx

local uuid = require "uuid"
local Model = require "app.shared.model_extended".ModelEX

uuid.randomseed( ngx.now() )

local Device = Model:extend("devices", {
    primary_key = "id",

    user = { model = lazy "app.user.model", relation = { "toOne", "id", "user_id" }  },
    guid = { model = lazy "app.guid.model", relation = { "morphOne", "x_id", "id" }  },

    validate = {
        { "name", exists = true, min_length = 1, max_length = 250 }
    },

    updateFilter = { name = true },
    exportFilter = { id = true, name = true, guid = true },

    onCreated = function ( self )
        local _ = self.guid.model():create{ x_id = self.id, x_type = "devices" }
    end
})

return Device