--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 13.02.2016
-- Time: 16:00
-- To change this template use File | Settings | File Templates.
--
local BaseController = require "app.shared.base_controller"

local DeviceController = setmetatable( {}, { __index = BaseController } )

DeviceController.base = BaseController
DeviceController.model = lazy "app.device.model"

return DeviceController