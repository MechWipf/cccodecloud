--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 14.09.2015
-- Time: 17:36
-- To change this template use File | Settings | File Templates.
--

require "libs.linq"

--local validate = require "lapis.validate"
local class = require "app.shared.class"
local lapis = require "lapis"
--local json = require "cjson"
local respond_to = lapis.application.respond_to

-- debug
--local utils = require "libs.utils"
--

local Router = class {

    __construct = function ( self, app )
        self.app = app

        self.routes = {}
    end,

    route = function ( self, baseUrl, routeName, controller )
        local app = self.app
        local url

        url = "/"..baseUrl.."/"..routeName
        app:match( url, respond_to( self:resolve( controller ) ) )

        url = "/"..baseUrl.."/"..routeName.."/:id"
        app:match( url, respond_to( self:resolve( controller ) ) )

        self.routes[baseUrl .. "." .. routeName] = controller
    end,

    resolve = function ( _, controller )
        local c = {
            GET = function( self )
                return self.params.id and controller:get( self, self.params.id, self.params) or controller:index(self, self.params)
            end,

            POST = function( self )
                return controller:save( self, self.params )
            end,

            DELETE = function( self )
                return self.params.id and controller:delete( self, self.params.id, self.params ) or {}
            end
        }

        c.PUT = c.POST

        return c
    end,
}

return Router