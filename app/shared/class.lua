--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 14.09.2015
-- Time: 17:42
-- To change this template use File | Settings | File Templates.
--

local function class(prototype)
    local metatable = { __index = prototype }
    prototype.__metatable = metatable
    return function(...)
        local self = setmetatable({}, metatable)
        if self.__construct then self:__construct(...) end
        return self
    end
end

return class