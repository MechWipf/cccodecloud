--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 14.02.2016
-- Time: 01:34
-- To change this template use File | Settings | File Templates.
--
local from = from

local validate = require "lapis.validate"
local permission = require "app.user.permission"
local Resource = require "app.shared.resource"
local json = require "cjson"

local BaseController = {}

local function exportCollection ( collection )
    return from( collection ):map( function ( v, col ) col[#col + 1] = v:export() return col end, {} )
end

local function addPermissions ( resource, permissions, user )
    local _firstChain = true
    local chain = function () if _firstChain then return "" else return "or" end end

    if #permissions > 0 then
        for _, v in pairs( permissions ) do
            if v.filter == "*" then return resource end
        end
    end

    if #permissions > 0 then
        resource:wherePOpen( "and" )

        for _, v in pairs( permissions ) do
            local v = v.filter
            if v == "own" then
                resource:where{ chain = chain(), "user_id", user.id }
            elseif type(v) == "table" then
                resource:whereIn{ chain = chain(), "id", v }
            elseif type(v) == "number" then
                resource:where{ chain = chain(), "id", v }
            end
        end

        resource:wherePClose()
    end

    return resource
end

local function getWith ( o, model, user, with )
    for _, relation in pairs( with ) do
        local r_model = model[relation].model()
        local r_resource = Resource.newRelated( o, r_model, model[relation].relation )

        local r_permissions = permission.get( user, r_model.__class.__name, { "read" } )

        if r_permissions then
            addPermissions( r_resource, r_permissions, user )


            local type = model[relation].relation[1]
            local v

            if type == "toOne"
                    or type == "hasOne"
                    or type == "morphOne"
            then
                v = r_resource:first()
            elseif model[relation].relation[1] == "toMany"
                    or type == "morphMany"
                    or type == "hasMany"
            then
                v = r_resource:get()
            end

            o[relation] = v
        end
    end
end

function BaseController:index ( _self, values )
    local app = _self.app
    local model = self.model()
    local with = values.with and json.decode( values.with ) or nil

    local resource = Resource.new( model )

    local permissions = permission.get( app.user, model.__class.__name, { "read" } )

    if not ( #permissions > 0 ) then
        self.err = true
        return { json = { message = "No Permission" }, status = 403 }
    end

    addPermissions( resource, permissions, app.user )

    local list = resource:get()
    if #list > 0 then

        if with and #with > 0 then
            for _, o in pairs( list ) do
               getWith( o, model, app.user, with )
            end
        end

        self.list = list
        self.err = false
        return { json = exportCollection( list ) }
    else
        _self:write("[]")
        self.err = false
        return { content_type = "application/json", layout = false }
    end
end

function BaseController:get ( _self, idx, values )
    local app = _self.app
    local model = self.model()
    local permissions = permission.get( app.user, model.__class.__name, { "read" }, { idx, "own" } )
    local with = values.with and json.decode( values.with ) or nil

    local resource = Resource.new( model )

    if not ( #permissions > 0 ) then
        self.err = true
        return { json = { message = "No Permission" }, status = 403 }
    end

    addPermissions( resource, permissions, app.user )

    resource:where{ chain = "and", "id", idx }

    local one = resource:first()

    if not one then
        self.err = true
        return { json = { message = "Resource not found." }, status = 404 }
    end

    if with and #with > 0 then
        getWith( one, model, app.user, with )
    end

    self.one = one
    self.err = false
    return { json = one:export() }
end

function BaseController:save ( _self, values )
    local app = _self.app
    local model = self.model()

    if values.id then
        local with = values.with and json.decode( values.with ) or nil
        values.with = nil

        local resource = Resource.new( model )

        local permissions = permission.get( app.user, model.__class.__name, { "update" }, { values.id, "own" } )

        if not ( #permissions > 0 ) then
            return { json = { message = "No Permission" }, status = 403 }
        end

        addPermissions( resource, permissions, app.user )


        local one = resource:where({ "id", values.id }):first()

        if not one then
            self.err = true
            return { json = { message = "Id invalid." }, status = 400 }
        end

        if model.validate then
            validate.assert_valid( values, model.validate )
        end

        local update = {}
        for k, v in pairs( values ) do
            if model.updateFilter and model.updateFilter[k] then
                update[k] = v
            end
        end

        one:update( update )

        if with and #with > 0 then
            getWith( one, model, app.user, with )
        end

        self.one = one
        self.err = false
        return { json = one:export() }
    end

    return self:create( _self, values )
end

function BaseController:create( _self, values )
    local app = _self.app
    local model = self.model()
    local with = values.with and json.decode( values.with ) or nil
    values.with = nil

    local permissions = permission.get( app.user, model.__class.__name, { "create" } )

    if not ( #permissions > 0 ) then
        self.err = true
        return { json = { message = "No Permission" }, status = 403 }
    end

    values.user_id = app.user.id

    if model.validate then
        validate.assert_valid( values, model.validate )
    end

    local one = model:create( values )

    if with and #with > 0 then
        getWith( one, model, app.user, with )
    end

    self.one = one
    self.err = false
    return { json = one:export(), status = 201 }
end

function BaseController:delete ( _self, idx )
    local app = _self.app
    local model = self.model()

    local resource = Resource.new( model )

    local permissions = permission.get( app.user, model.__class.__name, { "delete" }, { idx, "own" } )

    if not ( #permissions > 0 ) then
        self.err = true
        return { json = { message = "No Permission" }, status = 403 }
    end

    addPermissions( resource, permissions, app.user )

    local one = resource:where({ "id", idx }):first()

    if not one then
        self.err = true
        return { json = { message = "Id invalid." }, status = 400 }
    end

    local one_id = one.id
    one:delete()

    self.one = one
    self.err = false
    return { json = { id = one_id } }
end

return BaseController