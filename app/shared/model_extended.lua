local Model
Model = require("lapis.db.model").Model
local ModelEX
do
  local _parent_0 = Model
  local _base_0 = {
    create = function(self, ...)
      local one
      if self.onCreate then
        local ok, cancel = pcall(self.onCreate, ...)
        if not ok then
          error(cancel)
        elseif not cancel then
          return error("Creation canceled by onCreate.")
        end
      end
      one = _parent_0.create(self, ...)
      if self.onCreated then
        local ok, _ = pcall(self.onCreated, one, ...)
        if not ok then
          print("Error in onCreated Method." .. tostring(_))
        end
      end
      return one
    end,
    update = function(self, ...)
      local one
      if self.onUpdate then
        local ok, cancel = pcall(self.onUpdate, ...)
        if not ok then
          error(cancel)
        elseif not cancel then
          return error("Creation canceled by onUpdate.")
        end
      end
      one = _parent_0.update(self, ...)
      if self.onUpdated then
        local ok, _ = pcall(self.onUpdated, one, ...)
        if not ok then
          print("Error in onUpdated Method." .. tostring(_))
        end
      end
      return one
    end,
    save = function(self, ...)
      local one
      if self.onSave then
        local ok, cancel = pcall(self.onSave, ...)
        if not ok then
          error(cancel)
        elseif not cancel then
          return error("Creation canceled by onSave.")
        end
      end
      one = _parent_0.save(self, ...)
      if self.onSaved then
        local ok, _ = pcall(self.onSaved, one, ...)
        if not ok then
          print("Error in onSaved Method." .. tostring(_))
        end
      end
      return one
    end,
    export = function(self)
      local rt = { }
      if not self.exportFilter and not #(self) > 0 then
        return rt
      end
      for k, v in pairs(self) do
        if type(v) == "table" and (v.exportFilter or #(self) > 0) then
          rt[k] = v:export()
        elseif self.exportFilter[k] then
          rt[k] = v
        end
      end
      return rt
    end
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__base)
  local _class_0 = setmetatable({
    __init = function(self, ...)
      return _parent_0.__init(self, ...)
    end,
    __base = _base_0,
    __name = "ModelEX",
    __parent = _parent_0
  }, {
    __index = function(cls, name)
      local val = rawget(_base_0, name)
      if val == nil then
        return _parent_0[name]
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  if _parent_0.__inherited then
    _parent_0.__inherited(_parent_0, _class_0)
  end
  ModelEX = _class_0
end
return {
  ModelEX = ModelEX
}
