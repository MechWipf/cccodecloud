--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 22.01.2016
-- Time: 21:55
-- To change this template use File | Settings | File Templates.
--
local concat = table.concat
local tostring = tostring
local format = string.format
local db = require "lapis.db"
local type = type
local setmetatable = setmetatable
--local pcall = pcall
local pairs = pairs

--Linq
require "libs.linq"
local from = from
-- end Linq

local Resource = {}
local resource_mt = { __index = Resource }

function Resource.new ( object )
    local self = setmetatable( {}, resource_mt)

    self.object = object

    return self
end

function Resource.newRelated ( parent, object, relation )
    local self = Resource.new( object )

    local object_name = object.__class.__name

    if relation[1] == "toOne" then
        self:whereRaw{ "%q.%q = '%s'", object_name, relation[3], parent[relation[2]] }
    elseif relation[1] == "hasMany" then
        self:whereRaw{ "%q.%q = '%s'", object_name, relation[3], parent[relation[2]] }
    elseif relation[1] == "morphOne" then
        self:join( parent, relation[3], relation[2], { "and", relation[3], parent[relation[3]] } )
    elseif relation[1] == "morphMany" then
        self:join( object, relation[3], relation[2], { "and", relation[3], parent[relation[3]] } )
    end

    return self
end

local function resource_build_where ( where_s, chain, t )
    return concat( {
        (where_s and concat( { where_s, chain:upper() }, " " ) or "WHERE" ),
        concat( t, " " )
    }, " " )
end


function Resource:where ( e1, q, e2 )
    local sb = {}
    local chain

    if type(e1) == "table" then
        chain, e1, q, e2 = e1.chain or "and", e1[1], e1[2], e1[3]
    end

    if not e2 then
        e2,q = q, "="
    end

    sb[#sb+1] = ("%q"):format( tostring(e1) )
    sb[#sb+1] = q
    sb[#sb+1] = tostring(e2)

    self.where_s = resource_build_where( self.where_s, chain, sb )

    return self
end


function Resource:whereRaw ( s )
    local sb = {}
    local chain

    if type(s) == "table" then
        chain, s = s.chain or "AND", s[1]
    end

    sb[#sb+1] = s

    self.where_s = resource_build_where( self.where_s, chain, sb )

    return self
end


function Resource:whereIn ( e1, t )
    local sb = {}
    local chain

    if type(e1) == "table" then
        chain, e1, t = e1.chain or "and", e1[1], e1[2]
    end

    sb[#sb+1] = ("%q"):format( tostring(e1) )
    sb[#sb+1] = "in"
    sb[#sb+1] = format( "(%s)", concat( t, "," ) )

    self.where_s = resource_build_where( self.where_s, chain, sb )

    return self
end

function Resource:wherePOpen ( chain )
    self.where_s = resource_build_where( self.where_s, chain, { "(" } )
end

function Resource:wherePClose ()
    self.where_s = self.where_s .. ")"
end

local function resource_map_columns ( obj, t )
    local t = t or {}
    local table_name = obj.table_name()

    from( obj:columns() ):map( function ( v, columns )
        local column_name = v.column_name

        columns[#columns+1] = format( "%q.%q as \"%s.%s\"", table_name, column_name, table_name, column_name)
        return columns
    end, t )
end


local function resource_get_objects ( t, obj, inner_obj )
    local match = format("^%s%%.", obj.table_name())
    local base = obj.__base

    return from( t ):map( function ( row, o )
        local obj = {}

        for k,v in pairs( row ) do
            if k:match( match ) then
                obj[k:gsub(match, "")] = v
            end
        end

        if inner_obj then
            for _, j in pairs( inner_obj ) do
                local name = j.table_name()

                obj[name] = resource_get_objects( { row }, j )[1]
            end
        end

        o[#o+1] = setmetatable( obj, base )
        return o
    end, {} )
end


local function resource_build_query ( self, first )
    local table_name = self.object.table_name()

    local cols = {}

    resource_map_columns( self.object, cols )

    if self.with_t then
        for _, with in pairs( self.with_t ) do
            resource_map_columns( with, cols )
        end
    end

    local sql = concat({
        concat( cols, ", " ),
        "FROM", format( "%q", table_name ),
        self.join_s or "",
        self.where_s or "",
        ( first and "limit 1" or "" )
    }, " ")

    local sql_result = db.select( sql )

    local rt = resource_get_objects( sql_result, self.object )

--    if self.with_t then
--        for _, with in pairs( self.with_t ) do
--            if not self.object[with.table_name()] then error "Nope!" end
--        end
--    end

    if first then
        rt = rt[1]
    end

    return rt
end


function Resource:get ()
    return resource_build_query( self, false )
end


function Resource:first ()
    return resource_build_query( self, true )
end


local function resource_build_join ( self, join_type, obj, reference_key, foreign_key, extra )
    local table_name = obj.__class.__name

    if extra then
        extra = format( " %s %q.%q = '%s'", extra[1]:upper(), table_name, extra[2], extra[3] )
    end

    self.join_s = concat({
        ( self.join_s or "" ),
        format( "%s JOIN %q ON %q.%q = %q.%q%s", join_type, table_name, table_name, reference_key, self.object.__class.__name, foreign_key, extra or "" )
    }, " ")

    return self
end


function Resource:join ( obj, reference_key, foreign_key, extra )
    return resource_build_join( self, "INNER", obj, reference_key, foreign_key, extra )
end


function Resource:leftJoin ( obj, reference_key, foreign_key, extra )
    return resource_build_join( self, "LEFT", obj, reference_key, foreign_key, extra )
end


function Resource:with ( object )
    self.with_t = self.with_t or {}
    self.with_t[#self.with_t+1] = object

    return self
end

return setmetatable( {}, { __index = Resource, __call = Resource.new } )