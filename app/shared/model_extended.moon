
import Model from require "lapis.db.model"

class ModelEX extends Model
  create: (...) =>
    local one

    if @onCreate then
      ok, cancel = pcall( @onCreate, ... )

      if not ok then
        error( cancel )
      elseif not cancel then
        return error "Creation canceled by onCreate."

    one = super\create(...)

    if @onCreated then
      ok, _ = pcall( @onCreated, one, ... )

      if not ok then
        print( "Error in onCreated Method." .. tostring( _ ) )

    return one

  update: (...) =>
    local one

    if @onUpdate then
      ok, cancel = pcall( @onUpdate, ... )

      if not ok then
        error( cancel )
      elseif not cancel then
        return error "Creation canceled by onUpdate."

    one = super\update(...)

    if @onUpdated then
      ok, _ = pcall( @onUpdated, one, ... )

      if not ok then
        print( "Error in onUpdated Method." .. tostring( _ ) )

    return one

  save: (...) =>
    local one

    if @onSave then
      ok, cancel = pcall( @onSave, ... )

      if not ok then
        error( cancel )
      elseif not cancel then
        return error "Creation canceled by onSave."

    one = super\save(...)

    if @onSaved then
      ok, _ = pcall( @onSaved, one, ... )

      if not ok then
        print( "Error in onSaved Method." .. tostring( _ ) )

    return one

  export: () =>
    rt = {}

    if not @exportFilter and not #(@) > 0 then
      return rt

    for k,v in pairs @
      if type(v) == "table" and ( v.exportFilter or #(@) > 0 ) then
        rt[k] = v\export!
      elseif @exportFilter[k] then
        rt[k] = v

    return rt

{ :ModelEX }
