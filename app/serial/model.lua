--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 05.12.2015
-- Time: 15:22
-- To change this template use File | Settings | File Templates.
--

local lazy = require "libs.utils".lazyLoad
local Model = require "app.shared.model_extended".ModelEX

local Serial = Model:extend( "serials", {
    primary_key = "id",

    guid = { model = lazy "app.guid.model", relation = { "morphOne", "x_id", "id" } },

    onCreated = function( self )
        require "app.guid.model":create { x_id = self.id, x_type = "serials" }
    end
} )

return Serial