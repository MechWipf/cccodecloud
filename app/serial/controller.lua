--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 13.02.2016
-- Time: 16:00
-- To change this template use File | Settings | File Templates.
--
local BaseController = require "app.shared.base_controller"

local SerialController = setmetatable( {}, { __index = BaseController } )

SerialController.base = BaseController
SerialController.model = lazy "app.serial.model"

return SerialController