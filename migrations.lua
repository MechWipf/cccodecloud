--
-- Created by IntelliJ IDEA.
-- User: MechWipf
-- Date: 26.07.2015
-- Time: 21:00
-- To change this template use File | Settings | File Templates.
--

local db = require "lapis.db"
local schema = require "lapis.db.schema"
local types = schema.types

local varchar, integer, boolean = types.varchar, types.integer, types.boolean

return {
    [1] = function ()

        schema.create_table( "permission_types", {
            { "id"  , types.serial },
            { "name", varchar },

            "primary key(id)"
        }, { if_not_exists = true } )

        schema.create_table( "permissions", {
            { "id"                , types.serial },
            { "name"              , varchar },
            { "permission_type_id", integer },

            "primary key(id)"
        }, { if_not_exists = true } )

        schema.create_table( "groups", {
            { "id"  , types.serial },
            { "name", varchar },

            "primary key(id)"
        }, { if_not_exists = true } )

        schema.create_table( "users", {
            { "id"        , types.serial },
            { "name"      , varchar },
            { "password"  , varchar },
            { "deleted_at", types.date{ null = true } },

            "primary key(id)"
        }, { if_not_exists = true } )

        schema.create_table( "groups_users", {
            { "id"      , types.serial },
            { "group_id", types.foreign_key },
            { "user_id" , types.foreign_key },

            "primary key(id, group_id, user_id)"
        }, { if_not_exists = true } )

        schema.create_table( "devices", {
            { "id"     , types.serial },
            { "name"   , varchar },
            { "user_id", integer },

            "primary key(id)"
        }, { if_not_exists = true } )

        schema.create_table( "permissions_x", {
            { "id"           , types.serial },
            { "x_id"         , integer },
            { "permission_id", integer },
            { "x_type"       , varchar },
            { "filter"       , varchar },
            { "value"        , boolean },

            "primary key(id, x_id)"
        }, { if_not_exists = true } )

        schema.create_table( "guids", {
            { "id"    , types.serial },
            { "x_id"  , integer },
            { "x_type", varchar },
            { "guid"  , varchar },

            "primary key(id, x_id)"
        }, { if_not_exists = true } )

        schema.create_table( "serials", {
            { "id"        , types.serial },
            { "type"      , integer },
            { "data"      , varchar },
            { "deleted_at", types.date{ null = true } },

            "primary key(id)"
        }, { if_not_exists = true } )
    end,
    [2] = function ()

        local c_id = db.insert( "permission_types", { name = "create" }, "id" )[1].id
        local r_id = db.insert( "permission_types", { name = "read"   }, "id" )[1].id
        local u_id = db.insert( "permission_types", { name = "update" }, "id" )[1].id
        local d_id = db.insert( "permission_types", { name = "delete" }, "id" )[1].id

        db.insert( "permissions", { name = "permissions", permission_type_id = c_id } )
        db.insert( "permissions", { name = "permissions", permission_type_id = r_id } )
        db.insert( "permissions", { name = "permissions", permission_type_id = u_id } )
        db.insert( "permissions", { name = "permissions", permission_type_id = d_id } )

        db.insert( "permissions", { name = "devices", permission_type_id = c_id } )
        db.insert( "permissions", { name = "devices", permission_type_id = r_id } )
        db.insert( "permissions", { name = "devices", permission_type_id = u_id } )
        db.insert( "permissions", { name = "devices", permission_type_id = d_id } )

        db.insert( "permissions", { name = "serials", permission_type_id = c_id } )
        db.insert( "permissions", { name = "serials", permission_type_id = r_id } )
        db.insert( "permissions", { name = "serials", permission_type_id = u_id } )
        db.insert( "permissions", { name = "serials", permission_type_id = d_id } )

        db.insert( "permissions", { name = "files", permission_type_id = c_id } )
        db.insert( "permissions", { name = "files", permission_type_id = r_id } )
        db.insert( "permissions", { name = "files", permission_type_id = u_id } )
        db.insert( "permissions", { name = "files", permission_type_id = d_id } )

        db.insert( "permissions", { name = "users", permission_type_id = r_id } )
        db.insert( "permissions", { name = "users", permission_type_id = u_id } )
        db.insert( "permissions", { name = "users", permission_type_id = d_id } )

    end,
}